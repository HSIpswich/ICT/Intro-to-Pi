# Introduction to Raspberry Pi

### Linux commands

Command | Description

pwd | Displays the current working directory

```bash
pwd
```

ls <location/path> | folder listing optional location
```bash
ls
ls /home/hsipswich
```

ls -alh <location/path>| full folder listing including the hidden files & folders with an optional location
```bash
ls -hal
```

cd <folder name/path>| Change Directory to the folder or full path location
```bash
cd /home/HSIpswich
cd Documents/
```
cd .. | change to parent directory
```bash
cd ..
```

clear | clears the terminal
```bash
clear
```

mkdir <folder name / path> | Make a Directory with the folder name.
```bash
mkdir My_Stuff
```

rmdir <folder name / path> | Removes the directory named - can only be run from outside of the directory.
```bash
rmdir My_Stuff
```

nano <filename> | creates a text file with the simple nano text editor
```bash
nano my_file.txt
```

cat <filename> | writes the contents of the file to display
```bash
cat my_file.txt
```

touch <name> | create an empty file with name
```bash
touch newfile.txt
```
<div class="page-break"/>

mv X Y | Move / Rename file X to Y
```bash
mv file1.txt file2.txt
```

cp x y | Copy file X to Y
```bash
cp file1.txt /Documents/file1.txt
```

rm x | remove file x
```bash
rm file1.txt
```

rm -r y | remove folders and files from folder y inclusive and all sub direcorties and files.
```bash
rm -r My_Dir/
```

tree | Tree View of Directory Structure
```bash
tree /
```

sudo halt | Safely Shutdown then remove power once complete
```bash
sudo halt
```

__*NB: Avoid spaces when possible when naming files and folders on a linux system make use of underscores or hyphens in place of spaces or use monkeyCase *__

__*Tab will attempt to autocomplete a line*__

command 1 \| command 2 | Pipe takes output of command 1 as an input to command 2 can be used multiple times if needed.

```bash
cat file1.txt | sort > file2.txt
```
Shortcut | Description |
-----|-----
.  | This Directory
..  | Parent Directory one Directory Up the tree
~  | Logged in users home folder shortcut

### Access Control

Type, Owners, Groups, and World (Other)

D = Directory, blank is a file

R = 4,
W = 2,
X = 1

chmod 755 file1.txt

chmod 777 /var/www/indxe.html

chmod 600 /home

chmod 775 somefile.c

sudo - Super User Access to the System, like Administrator access on Windows.

Edited with visudo


<div class="page-break"/>


### Raspberry Pi GPIO

<img src=http://toptechboy.com/wp-content/uploads/2015/06/raspberry-pi-2-pinout.jpg>

<div class="page-break"/>

### Circuit Diagram

<img src=http://toptechboy.com/wp-content/uploads/2015/06/raspberry-pi-circuit.jpg>

<div class="page-break"/>

### Python, The Web & GPIO
app.py
```Python
import RPi.GPIO as GPIO
from Flask import Flask, render_template, request

GPIO.setmode(GPIO.BCM)

# Create a dictionary for the pins to store state and a simple name
pins = {
  24 : {'name':'Bathroom Light', 'state' : GPIO.LOW},
  25 : {'name' : 'Kitchen Light', 'state' : GPIO.LOW}
}

# Set each pin as output and set to off / LOW
for pin in pins:
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, GPIO.LOW)

@app.route("/")
def main():
  # For each pin, read the state and store in the dictionary:
  for pin in pins:
    pins[pin]['state'] = GPIO.input(pin)
  # Put the pin dictionary into the template data dictionary
  templateData = {
  'pins' : pins
  }
  # pass the template data into the template index.html and return to user.
  return render_template('index.html', **templateData)
    if __name__ == '__main__':

@app.route('/<changePin>/<action>')
def action(changePin, action):
  # Read Pin from URL
  changePin = int(changePin)
  # Get the Device Name from the pin number      
  deviceName = pins[changePin]['name']
  # Run physical action based on action provided.
  if action == "on":
    GPIO.output(changePin, GPIO.HIGH)
    message = "Turned " + deviceName + " on."
  if action == "off":
    GPIO.output(changePin, GPIO.LOW)
    message = "Turned " + deviceName + " off."
  if action == "toggle":
    GPIO.output(changePin, not GPIO.input(changePin))
    message = "Toggled " + deviceName + "."

  for pin in pins:
    pins[pin]['state'] = GPIO.input(pin)

  templateData = {
  'message' : message,
  'pins' : pins
  }
  return render_template('index.html', **templateData)

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=8080 debug=True)
```
<div class="page-break"/>

### Python, The Web & GPIO
##### index.html
```html
<!DOCTYPE html>
<head>
   <title>Current Status</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
   <h1>Device Listing and Status</h1>

   {% for pin in pins %}
   <h2>{{ pins[pin].name }}
   {% if pins[pin].state == true %}
      is currently <strong>on</strong></h2><div class="row"><div class="col-md-2">
      <a href="/{{pin}}/off" class="btn btn-block btn-lg btn-default" role="button">Turn off</a></div></div>
   {% else %}
      is currently <strong>off</strong></h2><div class="row"><div class="col-md-2">
      <a href="/{{pin}}/on" class="btn btn-block btn-lg btn-primary" role="button">Turn on</a></div></div>
   {% endif %}
   {% endfor %}
</body>
</html>
```

---

## Further Reading Resource

#### Kolban's Book on the Raspberry Pi
https://leanpub.com/pi
