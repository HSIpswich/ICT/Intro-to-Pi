## Python

### What is Python?

Python is a scripting language for applications that will run on computers.

Python is available for all platforms so therefore is also known as a cross platform language.

### Our First Python Application.

hello.py

```Python
print "Hello, World!"
```

### Our Second applications

Which.py

```Python
import sys

print (sys.version)
```

#### To run python applications, but with sudo for access to the GPIO.

```bash
python hello.py
```

```bash
python Which.py
```

### Python & The GPIO

GPIO_1.py

```Python

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setup(11,GPIO.OUT)

GPIO.output(11,True)
GPIO.output(11,False)

GPIO.cleanup()

```
<div class="page-break"/>

### More Python Fun

Blink.py

```python
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11,GPIO.OUT)

GPIO.output(11,True)
time.sleep(1)
GPIO.output(11,False)
time.sleep(1)
GPIO.output(11,True)
time.sleep(1)
GPIO.output(11,False)
time.sleep(1)

GPIO.cleanup()
```

### Python and loops, and checks.

##### For loop

```Python
for x in range (5):
  print (x)

```
##### While loops

```python
while (x=1):
  print (1)
```

##### If condition

value_Check.py

```python
x = 5

if x >= 5:
  print (x)
else
  print ("Out of Range.")
```
##### Input

Input.py

```Python
person = input ('What is your name? [exit to quit]' )
if person = 'exit':
  exit()
print ('Hello ', person, 'I am your computer.')
```
<div class="page-break"/>

### Raspberry Pi & PWM Control

pwm.py

```Python
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
LED = 11
GPIO.setup(LED,GPIO.OUT)

my_pwm = GPIO.PWM(LED,100)
my_pwm.start(0)
bright = 0

while (bright != 'exit'):
  bright = input("How bright do you want the LED? [0-7 or exit to quit]")
  if bright.lower() == 'exit':
    print ('Cleaning up GPIO and then Exiting...')
    my_pwm.stop()
    GPIO.cleanup()
    exit()
  else:
    if int(bright) > 7:
      print ('Invalid entry'+ bright + ', please try again.')
    else:
      print ('Lighting is set to brightness level '+ bright + '.')
      my_pwm.ChangeDutyCycle(2**bright)
```
<div class="page-break"/>

### Python, The Web & GPIO

app.py

```Python
import RPi.GPIO as GPIO
from Flask import Flask, render_template, request

GPIO.setmode(GPIO.BCM)

# Create a dictionary for the pins to store state and a simple name
pins = {
  24 : {'name':'Toaster', 'state' : GPIO.LOW},
  25 : {'name' : 'Coffee Machine', 'state' : GPIO.LOW}
}

# Set each pin as output and set to off / LOW
for pin in pins:
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, GPIO.LOW)

@app.route("/")
def main():
  # For each pin, read the state and store in the dictionary:
  for pin in pins:
    pins[pin]['state'] = GPIO.input(pin)
  # Put the pin dictionary into the template data dictionary
  templateData = {
  'pins' : pins
  }
  # pass the template data into the template index.html and return to user.
  return render_template('index.html', **templateData)
    if __name__ == '__main__':

@app.route('/<changePin>/<action>')
def action(changePin, action):
  # Read Pin from URL
  changePin = int(changePin)
  # Get the Device Name from the pin number
  deviceName = pins[changePin]['name']
  # Run physical action based on action provided.
  if action == "on":
    GPIO.output(changePin, GPIO.HIGH)
    message = "Turned " + deviceName + " on."
  if action == "off":
    GPIO.output(changePin, GPIO.LOW)
    message = "Turned " + deviceName + " off."
  if action == "toggle":
    GPIO.output(changePin, not GPIO.input(changePin))
    message = "Toggled " + deviceName + "."

  for pin in pins:
    pins[pin]['state'] = GPIO.input(pin)

  templateData = {
  'message' : message,
  'pins' : pins
  }
  return render_template('index.html', **templateData)

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=8080 debug=True)

```
<div class="page-break"/>
index.html

```html
<!DOCTYPE html>
<head>
   <title>Current Status</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
   <h1>Device Listing and Status</h1>

   {% for pin in pins %}
   <h2>{{ pins[pin].name }}
   {% if pins[pin].state == true %}
      is currently <strong>on</strong></h2><div class="row"><div class="col-md-2">
      <a href="/{{pin}}/off" class="btn btn-block btn-lg btn-default" role="button">Turn off</a></div></div>
   {% else %}
      is currently <strong>off</strong></h2><div class="row"><div class="col-md-2">
      <a href="/{{pin}}/on" class="btn btn-block btn-lg btn-primary" role="button">Turn on</a></div></div>
   {% endif %}
   {% endfor %}
</body>
</html>
```
<div class="page-break"/>
### Python, The Web & Sense Hat

app.py
```Python
from sense_hat import SenseHat
from Flask import Flask, render_template, request

sense = SenseHat()
temp = sense.get_temperature()
humidity = sense.get_humidity()

@app.route("/")
def main():
  return render_template('main.html')

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=8080 debug=True)
```

main.html
```html
<!DOCTYPE html>
<head>
  <title>RPi Web Server Statistics</title>
</head>
<body>
  <H1>RPi Web Server Weather Station</H1>
  <H3> Temperature is {{temp}} C </H3>
  <H3> Relative Humidity is {{humidity}} % </H3>
</body>
```
