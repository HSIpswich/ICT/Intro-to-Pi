HSIpswich Intro To Pi

This course covers an introduction to the Raspberry Pi, hardware and the GPIO as well as basic Linux and Python Programming.

On completion of the workshop particpants will be able to 

- Navigate around and make use of a Linux PC.
- Write basic progams with Python.
- Utilising the GPIO to interact with the world.
- Hardware Understanding and Identification.