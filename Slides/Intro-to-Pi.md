class: middle, center

<!-- Insert Logo here... -->
![](image\HS_Logo_WIDE_GoogleLogo.png)


# Introduction to Raspberry Pi

## Sesion 1

## Presenter: Robert Manietta & James Harris
### Cause Leader IT&T, President
### HSIpswich

<!-- Note: Housekeeping, emergency exits, toilets, amenaties, Reference Book Kolban's book on Raspberry Pi (2016). -->

---
class: middle, center

### What is a Raspberry Pi?

Put simply, the Raspberry Pi is a cheap computer that exposes pins for physical computing.

Greater horsepower then with the arduino and Networking abilities, in the approximate same form factor of the arduino borad.

It's assumed you know nothing at all and we will go from scratch through to making fully functional projects.

What will we need.

1. Raspberry Pi 3 Model B
2. Micro SD Card + Adapter of Micro Card Reader - Good one min. 8GB Prefferable 16GB +
3. Power Supply - 5V @ 2.5A micro usb
4. Another PC / Laptop
5. HSIpswich Pi Image ( Has VNC Server Enabled and Configured )

---
class: middle, center

### What is a Raspberry Pi Continued?

Pi is more interactive to the arduino, and has a full desktop environment in the case of the Pi a Linux Operating system.

We will learn today the basics required to become familiar with the linux operating system and simple commands that will be necessary to make use of the raspberry pi.

---
class: middle, center
# What can you use a Pi to do?

![](image/Pi/297a75d43c3faf0729efa702b63e4e1e.jpg)
<img src="image/Pi/638e47835f0250ad4c0c1cbc921108cf.jpg" width="236" height="177" />
<img src="image/Pi/FHHY3MIJ6IEY65V.MEDIUM.jpg" width="236" height="177" /> <br />
<img src="image\Pi\FMLMUHZHWNU6G3E.MEDIUM.jpg" width="236" height="177" />
<img src="image\Pi\gadget-art-shape-storybox-piratebox-438622-pxhere.com.jpg" width="236" height="177" />
<img src="image\Pi\pitop-1.jpg" width="236" height="177" /><br />
<img src="image\Pi\ftmpgrii0tv9ylh-large.jpg" width="400" />

---
class: middle, center
### Installing The Linux OS.
</br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/D2TISpT7yLI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

* Used with kind Thanks to Nathan From TechRelay, who kindly has licensed us to use the above video to demonstrate the most common installation method of the Raspberry PI Operating System/(s). *

---
class: middle, left
### How do we start Logging in

Login: pi Password: raspberry

### How will we do this:

- Using the hsipswich laptop before you login as the workshop user with the password "P@ssw0rd" without the quotes. 
- Launch VNC
- Connecting to the IP Address listed inside your raspberry pi box.
- login with the above pi credentials.

---
class: middle, left
### Let's Get Started with Linux Commands

Command | Description
-----|-----
pwd | Displays the current working directory

```bash
pwd
```

ls <location/path> | folder listing optional location
```bash
ls
ls /home/hsipswich
```

ls -alh <location/path>| full folder listing including the hidden files & folders with an optional location
```bash
ls -hal
```

cd <folder name/path>| Change Directory to the folder or full path location
```bash
cd /home/HSIpswich
cd Documents/
```

---
class: middle, left
### Linux Commands
Command | Description
-----|-----

cd .. | change to parent directory
```bash
cd ..
```

clear | clears the terminal
```bash
clear
```

mkdir <folder name / path> | Make a Directory with the folder name.
```bash
mkdir My_Stuff
```

rmdir <folder name / path> | Removes the directory named - can only be run from outside of the directory.
```bash
rmdir My_Stuff
```

---
class: middle, left
### Linux Commands
Command | Description
-----|-----


nano <filename> | creates a text file with the simple nano text editor
```bash
nano my_file.txt
```

cat <filename> | writes the contents of the file to display
```bash
cat my_file.txt
```

touch <name> | create an empty file with name
```bash
touch newfile.txt
```

---
class: middle, left
### Linux Commands
Command | Description
-----|-----

mv X Y | Move / Rename file X to Y
```bash
mv file1.txt file2.txt
```

cp x y | Copy file X to Y
```bash
cp file1.txt /Documents/file1.txt
```

rm x | remove file x
```bash
rm file1.txt
```

rm -r y | remove folders and files from folder y inclusive and all sub direcorties and files.
```bash
rm -r My_Dir/
```

---
class: middle, left
### Linux Commands
Command | Description
-----|-----
tree | Tree View of Directory Structure
```bash
tree /
```

sudo halt | Safely Shutdown then remove power once complete
```bash
sudo halt
```

__*NB: Avoid spaces when possible when naming files and folders on a linux system make use of underscores or hyphens in place of spaces or use monkeyCase *__

__*Tab will attempt to autocomplete a line*__

---
class: middle, left
### Linux Commands Extention

#### PIPES

command 1 \| command 2 | Pipe takes output of command 1 as an input to command 2 can be used multiple times if needed.

```bash
cat file1.txt | sort > file2.txt
```
Shortcut | Description |
-----|-----
.  | This Directory
..  | Parent Directory one Directory Up the tree
~  | Logged in users home folder shortcut

---
class: middle, left
### Access Control

Type, Owners, Groups, and World (Other)

D = Directory, blank is a file

R = 4,
W = 2,
X = 1

chmod 755 file1.txt

chmod 777 /var/www/indxe.html

chmod 600 /home

chmod 775 somefile.c

---
class: middle, left
### Access Control

sudo - Super User Access to the System, like Administrator access on Windows.

Edited with visudo
---
class: middle, center

## Break

### Until

HS Ipswich welcomes inderviduals to come by use our equipment if need be and try and practice these skills.
</br>
<hr>
</br>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.

---

class: middle, center

<!-- Insert Logo here... -->
![](image\HS_Logo_WIDE_GoogleLogo.png)


# Introduction to Raspberry Pi

## Sesion 2

## Presenter: Robert Manietta & James Harris
### Cause Leader IT&T, President
### HSIpswich

<!-- Note: Housekeeping, emergency exits, toilets, amenaties, Reference Book Kolban's book on Raspberry Pi (2016). -->

---

class: middle, center

## Python

### What is Python?

Python is a scripting language for applications that will run on computers.

Python is available for all platforms so therefore is also known as a cross platform language.

---
class: middle

### Python Files

hello.py
```Python
print "Hello, World!"
```

Which.py
```Python
import sys

print (sys.version)
```
<!-- Note subtle differences between versions 2 and 3 ie () -->
To run python applications, but with sudo for access to the GPIO.

```bash
python hello.py
```

```bash
python Which.py
```

---
### Raspberry Pi GPIO

<img src=http://toptechboy.com/wp-content/uploads/2015/06/raspberry-pi-2-pinout.jpg width=640 height=480>

---
class: middle

### Python & GPIO

Let's start with a simple blink example. To blink an LED.

Circuit as below, requires 330Ohm Resistor and LED

GND = Pin 9
IO Pin = Pin 11

<img src=http://toptechboy.com/wp-content/uploads/2015/06/raspberry-pi-circuit.jpg width=640 height=480>

---

### Python & GPIO

```Python

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setup(11,GPIO.OUT)

GPIO.output(11,True)
GPIO.output(11,False)

GPIO.cleanup()

```

---
class: middle

# More Python Fun

Blink.py
```python
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setup(11,GPIO.OUT)

GPIO.output(11,True)
time.sleep(1)
GPIO.output(11,False)
time.sleep(1)
GPIO.output(11,True)
time.sleep(1)
GPIO.output(11,False)
time.sleep(1)

GPIO.cleanup()
```
---
class: middle

### Python and loops, and checks.

##### For loop

```Python
for x in range (5):
  print (x)

```
##### While loops

```python
while (x=1):
  print (1)
```
---
class: middle

### Python and loops, and checks.

##### If condition

```python
if x >= 9:
  print (x)
else
  print ("Out of Range.")
```
##### Input

```Python
person = input ('What is your name? [exit to quit]' )
if person = 'exit':
  exit()
print ('Hello ', person, 'I am your computer.')
```
<!-- Add some self exploration project for the group to try on their own maybe touch on a for loop in python and see if they can make it flash to a provided number of iterations. -->

---
class: middle

### Raspberry Pi & PWM Control

pwm.py
```Python
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
LED = 11
GPIO.setup(LED,GPIO.OUT)

my_pwm = GPIO.PWM(LED,100)
my_pwm.start(0)
bright = 0

while (bright != 'exit'):
  bright = input("How bright do you want the LED? [0-7 or exit to quit]")
  if bright.lower() == 'exit':
    print ('Cleaning up GPIO and then Exiting...')
    my_pwm.stop()
    GPIO.cleanup()
    exit()
  else:
    if int(bright) > 7:
      print ('Invalid entry'+ bright + ', please try again.')
    else:
      print ('Lighting is set to brightness level '+ bright + '.')
      my_pwm.ChangeDutyCycle(2**bright)
```

---
class: middle
### Python, The Web & GPIO

##### Ensure we have the required packages.

```bash
sudo apt-get install python-pip python-flask sense-hat
sudo pip install flask
```

---
class: middle
### Python, The Web & GPIO

app.py
```Python
import RPi.GPIO as GPIO
from Flask import Flask, render_template, request

GPIO.setmode(GPIO.BCM)

# Create a dictionary for the pins to store state and a simple name
pins = {
  24 : {'name':'Toaster', 'state' : GPIO.LOW},
  25 : {'name' : 'Coffee Machine', 'state' : GPIO.LOW}
}

# Set each pin as output and set to off / LOW
for pin in pins:
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, GPIO.LOW)
```
---
app.py continued
```python
@app.route("/")
def main():
  # For each pin, read the state and store in the dictionary:
  for pin in pins:
    pins[pin]['state'] = GPIO.input(pin)
  # Put the pin dictionary into the template data dictionary
  templateData = {
  'pins' : pins
  }
  # pass the template data into the template index.html and return to user.
  return render_template('index.html', **templateData)
    if __name__ == '__main__':
```
---
app.py continued
```python
@app.route('/<changePin>/<action>')
def action(changePin, action):
  # Read Pin from URL
  changePin = int(changePin)
  # Get the Device Name from the pin number      
  deviceName = pins[changePin]['name']
  # Run physical action based on action provided.
  if action == "on":
    GPIO.output(changePin, GPIO.HIGH)
    message = "Turned " + deviceName + " on."
  if action == "off":
    GPIO.output(changePin, GPIO.LOW)
    message = "Turned " + deviceName + " off."
  if action == "toggle":
    GPIO.output(changePin, not GPIO.input(changePin))
    message = "Toggled " + deviceName + "."

  for pin in pins:
    pins[pin]['state'] = GPIO.input(pin)

  templateData = {
  'message' : message,
  'pins' : pins
  }
  return render_template('index.html', **templateData)

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=8080 debug=True)

```
---
class: middle
### Python, The Web & GPIO

##### index.html
```html
<!DOCTYPE html>
<head>
   <title>Current Status</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
   <h1>Device Listing and Status</h1>

   {% for pin in pins %}
   <h2>{{ pins[pin].name }}
   {% if pins[pin].state == true %}
      is currently <strong>on</strong></h2><div class="row"><div class="col-md-2">
      <a href="/{{pin}}/off" class="btn btn-block btn-lg btn-default" role="button">Turn off</a></div></div>
   {% else %}
      is currently <strong>off</strong></h2><div class="row"><div class="col-md-2">
      <a href="/{{pin}}/on" class="btn btn-block btn-lg btn-primary" role="button">Turn on</a></div></div>
   {% endif %}
   {% endfor %}
</body>
</html>
```

---
class: middle
### Python, The Web & Sense Hat

```bash
sudo apt-get install python-pip python-flask sense-hat
sudo pip install flask
```

app.py
```Python
from sense_hat import SenseHat
from Flask import Flask, render_template, request

sense = SenseHat()
temp = sense.get_temperature()
humidity = sense.get_humidity()

@app.route("/")
def main():
  return render_template('main.html')

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=8080 debug=True)
```

---
class: middle

### Python, The Web & Sense Hat

main.html
```html
<!DOCTYPE html>
<head>
  <title>RPi Web Server Statistics</title>
</head>
<body>
  <H1>RPi Web Server Weather Station</H1>
  <H3> Temperature is {{temp}} C </H3>
  <H3> Relative Humidity is {{humidity}} % </H3>
</body>
```

---
class: middle, center

## You Made it To the End

### Thank You

HS Ipswich welcomes inderviduals to come by use our equipment if need be and try and practice these skills.
</br>
<hr>
</br>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
